package com.codefish.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 描述
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/23 下午 06:11
 */
@SpringBootApplication
@EnableFeignClients
@EnableSwagger2
public class ServerCustomerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServerCustomerApplication.class, args);
    }
}
