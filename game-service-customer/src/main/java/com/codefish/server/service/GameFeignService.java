package com.codefish.server.service;

import com.codefish.api.entities.AjaxResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author codefish
 * @version 1.0
 * @date 2022/05/23 下午 06:13
 */
@FeignClient("GAME-SERVICE-PROVIDER")
@RequestMapping("/game")
public interface GameFeignService {
    @GetMapping("/start")
    AjaxResult startGame();

    @PostMapping("/next")
    AjaxResult placePiece(@RequestParam("locationX") Integer locationX,
                          @RequestParam("locationY") Integer locationY,
                          @RequestParam("token") String token);

}
