package com.codefish.server.controller;

import com.codefish.api.controller.IGameController;
import com.codefish.api.entities.AjaxResult;
import com.codefish.server.service.GameFeignService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/23 下午 06:29
 */
@RestController
@RequestMapping("/proxy/game")
@Api(tags = "game")
public class BalancedGameController implements IGameController {
    @Autowired
    GameFeignService gameFeignService;

    @Override
    @GetMapping("/start")
    public AjaxResult startGame() {
        return gameFeignService.startGame();
    }

    @Override
    @PostMapping("/next")
    public AjaxResult placePiece(Integer locationX, Integer locationY, String token) {
        return gameFeignService.placePiece(locationX, locationY, token);
    }
}
