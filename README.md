## 游戏程序设计——象棋对战实验报告

### 题目描述：

将cnchess游戏中的AI部分，即alpha-beta剪枝部分的代码用java或者python实现， 即将棋谱搜索从客户端移至服务器端实现。先通过本地客户端的测试程序，再将js端的代码改成与服务器端通信的。

### 示例代码研究:

下载，解压，浏览器运行,可以看到这样的界面：<br>
![img.png](doc/img.png)<br>
帧不戳！先玩上个两局...<br/>
![img.png](doc/cnchess_fail.png)<br>
emm...这AI挺强,那就来研究研究它怎么写的。<br/>
_**过了几分钟...**_<br/>
本人太菜，没有js和算法基础，实在看不懂,告辞！

### ~~实验总结(雾)~~

~~通过本次实验，我深刻的认识到自己能力的不足,学习的道路还很漫长。~~ <br>
...<br>
...<br>
...<br>

_那总不能啥都不写吧，既然这个实验的核心是alpha-beta剪枝,那就拿个棋谱种数比较少的来入门吧。 说到这个世界上最简单的棋，那就是井字棋了。再结合最近在学的微服务框架,那不就高大上起来了吗...嘿嘿。OK,正片开始~_

## 井字棋对战——微服务版

### 游戏核心算法

参见[井字棋游戏核心介绍](./井字棋游戏核心介绍.md)

### 技术栈

<table>
<tr style="font-weight: bolder;background: grey"><td>名称</td><td>说明</td><td>官网</td></tr>
<tr><td>SpringBoot</td><td>MVC框架，提供Rest服务</td>
<td> <a href="https://spring.io/projects/spring-boot">https://spring.io/projects/spring-boot</a> </td></tr>
<tr><td>SpringCloud</td><td>微服务框架</td>
<td><a href="https://spring.io/projects/spring-cloud/">https://spring.io/projects/spring-cloud/</a></td>  </tr>
<tr><td>eureka</td><td>服务注册中心，用于注册微服务</td>
<td><a href="https://www.eurekanetwork.org/">https://www.eurekanetwork.org/</a></td> </tr>
<tr><td>openFeign</td><td>微服务Rest远程调用</td>
<td><a href="https://spring.io/projects/spring-cloud-openfeign">https://spring.io/projects/spring-cloud-openfeign</a></td> </tr>
<tr><td>MySQL</td><td>关系型数据库，记录游戏对局记录</td>
<td><a href="https://www.mysql.com/">https://www.mysql.com/</a></td> </tr>
<tr><td>Redis</td><td>key-value型NoSQL,存储游戏对局状态</td>
<td><a href="https://redis.io/">https://redis.io/</a></td> </tr>
<tr><td>RabbitMQ</td><td>消息队列，提供游戏对局超时结束功能</td>
<td><a href="https://www.rabbitmq.com/">https://www.rabbitmq.com/</a></td> </tr>
</table>

### 模块介绍

<table>
<tr style="font-weight: bolder;background: grey"><td>模块名称</td><td>模块描述</td></tr>
<tr><td>game-core</td><td>井字棋游戏核心模块</td></tr>
<tr><td>common-api</td><td>实体类、接口等通用模块</td></tr>
<tr><td>discovery-eureka-server</td><td>eureka微注册服务模块</td></tr>
<tr><td>game-service-provider</td><td>游戏服务业务逻辑模块</td></tr>
<tr><td>game-service-customer</td><td>微服务调用模块</td></tr>
</table>



