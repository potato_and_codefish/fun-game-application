package com.codefish.game.core.piece;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 棋子抽象类
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 01:33
 */
@Data
@AllArgsConstructor
public class AbstractPiece {
    /**
     * 棋子名字(显示名称)
     */
    String name;


}
