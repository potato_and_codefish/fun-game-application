package com.codefish.game.core.chess;

import com.codefish.game.core.piece.AbstractPiece;
import lombok.Data;

import java.util.List;


/**
 * 棋谱布局抽象类
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 01:29
 */
@Data
public abstract class AbstractGrid {


    //game code
    public static final int RED_WIN = 0;
    public static final int BLUE_WIN = 1;
    public static final int NO_WIN = 2;
    public static final int CANT_JUDGE = 3;

    /**
     * 棋谱高度
     */
    int height;
    /**
     * 棋谱宽度
     */
    int width;
    /**
     * 当前棋盘节点布局
     */
    AbstractPiece[][] pieces;
    /**
     * 红方（red team）可用棋子列表
     */
    List<AbstractPiece> redPieceList;
    /**
     * 蓝方（blue team）可用棋子列表
     */
    List<AbstractPiece> bluePieceList;


    public AbstractGrid(int height, int width) {
        this.height = height;
        this.width = width;
        pieces = new AbstractPiece[this.height + 1][this.width + 1];
    }

    /**
     * 评估当前布局对红色方的优势得分
     *
     * @return 当前布局对红色方的优势得分
     */
    public abstract int evaluateRedPoint();

    /**
     * 评估当前布局对蓝色方的优势得分
     *
     * @return 当前布局对蓝色方的优势得分
     */
    public abstract int evaluateBluePoint();

    /**
     * 打印当前棋盘布局
     */
    public abstract void print();


    /**
     * 判定当前棋盘胜负情况(0 红方胜利 ,1蓝方胜利,2平局)
     *
     * @return 胜负情况
     */
    public abstract int judge();


    /**
     * 悔棋操作
     *
     * @param px px
     * @param py py
     */
    public void removePiece(int px, int py) {
        if (!checkPosition(px, py)) {
            throw new IllegalArgumentException("越出棋盘边界");
        }
        if (pieces[px][py] == null) {
            throw new RuntimeException("无法尝试移走不存在的棋子");
        }
        pieces[px][py] = null;
    }

    /**
     * 下棋
     *
     * @param px    px
     * @param py    py
     * @param piece 棋子
     */
    public void placePiece(int px, int py, AbstractPiece piece) {
        if (!checkPosition(px, py)) {
            throw new IllegalArgumentException("越出棋盘边界");
        }
        if (pieces[px][py] != null) {
            throw new RuntimeException("无法尝试在已有棋子的格子上放置棋子");
        }
        pieces[px][py] = piece;
    }


    /**
     * 检查操作是否在棋盘内
     *
     * @param px px
     * @param py py
     * @return 检查结果
     */
    private boolean checkPosition(int px, int py) {
        return px > 0 && px <= height && py > 0 && py <= width;
    }


}
