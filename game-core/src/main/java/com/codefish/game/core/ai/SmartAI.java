package com.codefish.game.core.ai;


import com.codefish.game.core.chess.TicTacToeGrid;
import com.codefish.game.core.piece.AbstractPiece;
import com.codefish.game.core.piece.TacPiece;
import com.codefish.game.core.piece.TicPiece;

/**
 * AI核心类
 * (玩家先手，做为红方，电脑后手，做为蓝方)
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 03:03
 */

public class SmartAI {
    private static final int COM_SIDE = 0;
    private static final int PLAYER_SIDE = 1;


    public SmartAI() {

    }


    public int[] nextStep(TicTacToeGrid grid) {

        Point nextP = findMaxMin(grid, 0);

//        System.out.print("AI计算结果(AI落子x轴,AI落子y轴,评估值):" + nextP);
//        System.out.println();
        if (nextP.x == -1) {
            return new int[]{-1, -1};
        }
        grid.placePiece(nextP.x, nextP.y, new TacPiece());
        return new int[]{nextP.x, nextP.y};
    }

    private Point findMaxMin(TicTacToeGrid nowGrid, int whoseSide) {
        if (nowGrid.judge() != TicTacToeGrid.CANT_JUDGE) {
            // System.out.println(nowGrid.evaluateRedPoint() + " " + nowGrid.evaluateBluePoint());
            return new Point(-1, -1, countValue(nowGrid.evaluateRedPoint(), nowGrid.evaluateBluePoint()));
        }

        AbstractPiece[][] pieces = nowGrid.getPieces();

        if (whoseSide == COM_SIDE) {
            //电脑方下棋(需要寻找极大值)
            Point maxp = new Point(-1, -1, Integer.MIN_VALUE);
            for (int i = 1; i <= nowGrid.getHeight(); i++) {
                for (int j = 1; j <= nowGrid.getWidth(); j++) {
                    if (pieces[i][j] == null) {
                        nowGrid.placePiece(i, j, new TacPiece());
                        //交由玩家方决定
                        Point p = findMaxMin(nowGrid, 1 - whoseSide);
                        //取最大
                        if (p.val >= maxp.val) {
                            maxp = p;
                            maxp.x = i;
                            maxp.y = j;
                        }
                        nowGrid.removePiece(i, j);
                    }
                }
            }
            return maxp;
        } else {
            //玩家方下棋(需要寻找级小值)
            Point minp = new Point(-1, -1, Integer.MAX_VALUE);
            for (int i = 1; i <= nowGrid.getHeight(); i++) {
                for (int j = 1; j <= nowGrid.getWidth(); j++) {
                    if (pieces[i][j] == null) {
                        nowGrid.placePiece(i, j, new TicPiece());
                        //交由电脑方决定
                        Point p = findMaxMin(nowGrid, 1 - whoseSide);
                        //取最小
                        if (p.val <= minp.val) {
                            minp = p;
                            minp.x = i;
                            minp.y = j;
                        }
                        nowGrid.removePiece(i, j);
                    }
                }
            }
            return minp;
        }
    }


    private int countValue(int redScore, int blueScore) {
        return blueScore - redScore;
    }


    private class Point {
        int x;
        int y;
        int val;


        public Point(int x, int y, int val) {
            this.x = x;
            this.y = y;
            this.val = val;
        }

        @Override
        public String toString() {
            return "(" + x + "," + y + "," + val + ")";
        }
    }

}
