package com.codefish.game.core.chess;

import com.codefish.game.core.piece.TacPiece;
import com.codefish.game.core.piece.TicPiece;

import java.util.ArrayList;

/**
 * 井字棋棋谱
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 01:57
 */
public class TicTacToeGrid extends AbstractGrid {

    public TicTacToeGrid() {
        super(3, 3);
        redPieceList = new ArrayList<>();
        redPieceList.add(new TicPiece());
        bluePieceList = new ArrayList<>();
        bluePieceList.add(new TacPiece());
    }

    @Override
    public int evaluateRedPoint() {
        //评分计数器
        int ans = 0;
        //获取双方棋子标识
        String redPieceName = redPieceList.get(0).getName();
        String bluePieceName = bluePieceList.get(0).getName();
        //水平连子评估
        for (int i = 1; i < pieces.length; i++) {
            boolean flag = false;
            //统计路线上A出现次数
            int count = 0;
            for (int j = 1; j < pieces[i].length; j++) {
                if (pieces[i][j] != null && redPieceName.equals(pieces[i][j].getName())) {
                    flag = true;
                    count++;
                } else if (pieces[i][j] != null && bluePieceName.equals(pieces[i][j].getName())) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                //出现3连子，自身绝对优势
                if (count == 3) return Integer.MAX_VALUE;
                ans += count * count;
            }
        }
        //垂直连子评估
        for (int j = 1; j < pieces[0].length; j++) {
            boolean flag = false;
            int count = 0;
            for (int i = 1; i < pieces.length; i++) {
                if (pieces[i][j] != null && redPieceName.equals(pieces[i][j].getName())) {
                    flag = true;
                    count++;
                } else if (pieces[i][j] != null && bluePieceName.equals(pieces[i][j].getName())) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                //出现3连子，自身绝对优势
                if (count == 3) return Integer.MAX_VALUE;
                ans += count * count;
            }
        }
        //两条对角线评估
        boolean flag = false;
        int count = 0;
        for (int i = 1; i < pieces.length; i++) {
            if (pieces[i][i] != null && redPieceName.equals(pieces[i][i].getName())) {
                flag = true;
                count++;
            } else if (pieces[i][i] != null && bluePieceName.equals(pieces[i][i].getName())) {
                flag = false;
                break;
            }
        }
        if (flag) {
            //出现3连子，自身绝对优势
            if (count == 3) return Integer.MAX_VALUE;
            ans += count * count;
        }
        flag = false;
        count = 0;
        for (int i = 1; i < pieces.length; i++) {
            int j = pieces.length - i;
            if (pieces[i][j] != null && redPieceName.equals(pieces[i][j].getName())) {
                flag = true;
                count++;
            } else if (pieces[i][j] != null && bluePieceName.equals(pieces[i][j].getName())) {
                flag = false;
                break;
            }
        }
        if (flag) {
            //出现3连子，自身绝对优势
            if (count == 3) return Integer.MAX_VALUE;
            ans += count * count;
        }

        return ans;
    }


    @Override
    public int evaluateBluePoint() {
        //评分计数器
        int ans = 0;
        //获取双方棋子标识
        String redPieceName = redPieceList.get(0).getName();
        String bluePieceName = bluePieceList.get(0).getName();
        //水平连子评估
        for (int i = 1; i < pieces.length; i++) {
            boolean flag = false;
            int count = 0;
            for (int j = 1; j < pieces[i].length; j++) {
                if (pieces[i][j] != null && bluePieceName.equals(pieces[i][j].getName())) {
                    flag = true;
                    count++;
                } else if (pieces[i][j] != null && redPieceName.equals(pieces[i][j].getName())) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                //出现3连子，自身绝对优势
                if (count == 3) return Integer.MAX_VALUE;
                ans += count * count;
            }
        }
        //垂直连子评估
        for (int j = 1; j < pieces[0].length; j++) {
            boolean flag = false;
            int count = 0;
            for (int i = 1; i < pieces.length; i++) {
                if (pieces[i][j] != null && bluePieceName.equals(pieces[i][j].getName())) {
                    flag = true;
                    count++;
                } else if (pieces[i][j] != null && redPieceName.equals(pieces[i][j].getName())) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                //出现3连子，自身绝对优势
                if (count == 3) return Integer.MAX_VALUE;
                ans += count * count;
            }
        }
        //两条对角线评估
        boolean flag = false;
        int count = 0;
        for (int i = 1; i < pieces.length; i++) {
            if (pieces[i][i] != null && bluePieceName.equals(pieces[i][i].getName())) {
                flag = true;
                count++;
            } else if (pieces[i][i] != null && redPieceName.equals(pieces[i][i].getName())) {
                flag = false;
                break;
            }
        }
        if (flag) {
            //出现3连子，自身绝对优势
            if (count == 3) return Integer.MAX_VALUE;
            ans += count * count;
        }
        flag = false;
        count = 0;
        for (int i = 1; i < pieces.length; i++) {
            int j = pieces.length - i;
            if (pieces[i][j] != null && bluePieceName.equals(pieces[i][j].getName())) {
                flag = true;
                count++;
            } else if (pieces[i][j] != null && redPieceName.equals(pieces[i][j].getName())) {
                flag = false;
                break;
            }
        }
        if (flag) {
            //出现3连子，自身绝对优势
            if (count == 3) return Integer.MAX_VALUE;
            ans += count * count;
        }
        return ans;
    }


    @Override
    public int judge() {
        String redPieceName = redPieceList.get(0).getName();
        String bluePieceName = bluePieceList.get(0).getName();
        String name = null;
        //是否可能平局
        boolean flag = true;
        int redCnt = 0;
        int blueCnt = 0;
        //水平方向
        for (int i = 1; i <= height; i++) {
            for (int j = 1; j <= width; j++) {
                if (pieces[i][j] == null) {
                    flag = false;
                    break;
                }
                if (pieces[i][j].getName().equals(redPieceName)) {
                    redCnt++;
                } else {
                    blueCnt++;
                }
            }
            if (redCnt == width) {
                return RED_WIN;
            }
            if (blueCnt == width) {
                return BLUE_WIN;
            }
            redCnt = 0;
            blueCnt = 0;
        }
        //竖直方向
        for (int j = 1; j <= width; j++) {
            for (int i = 1; i <= height; i++) {
                if (pieces[i][j] == null) {
                    flag = false;
                    break;
                }
                if (pieces[i][j].getName().equals(redPieceName)) {
                    redCnt++;
                } else {
                    blueCnt++;
                }
            }
            if (redCnt == height) {
                return RED_WIN;
            }
            if (blueCnt == height) {
                return BLUE_WIN;
            }
            redCnt = 0;
            blueCnt = 0;
        }
        //对角
        for (int i = 1; i <= height; i++) {
            if (pieces[i][i] == null) {
                flag = false;
                break;
            }
            if (pieces[i][i].getName().equals(redPieceName)) {
                redCnt++;
            } else {
                blueCnt++;
            }
        }
        if (redCnt == height) {
            return RED_WIN;
        }
        if (blueCnt == height) {
            return BLUE_WIN;
        }
        redCnt = 0;
        blueCnt = 0;

        for (int i = 1; i <= height; i++) {
            if (pieces[i][width - i + 1] == null) {
                flag = false;
                break;
            }
            if (pieces[i][width - i + 1].getName().equals(redPieceName)) {
                redCnt++;
            } else {
                blueCnt++;
            }
        }
        if (redCnt == height) {
            return RED_WIN;
        }
        if (blueCnt == height) {
            return BLUE_WIN;
        }
        redCnt = 0;
        blueCnt = 0;

        return flag ? NO_WIN : CANT_JUDGE;
    }

    @Override
    public void print() {
        for (int i = 1; i <= height; i++) {
            for (int j = 1; j <= width; j++) {
                System.out.print((pieces[i][j] == null ? "#" : pieces[i][j]) + " ");
            }
            System.out.println();
        }
    }

    public String[][] parse() {
        String[][] s = new String[height][width];
        for (int i = 1; i <= height; i++) {
            for (int j = 1; j <= width; j++) {
                s[i - 1][j - 1] = pieces[i][j] == null ? "#" : pieces[i][j].getName();
            }
        }
        return s;
    }

}
