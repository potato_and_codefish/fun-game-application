package com.codefish.game.core.utils;

import com.codefish.game.core.chess.TicTacToeGrid;
import com.codefish.game.core.piece.AbstractPiece;
import com.codefish.game.core.piece.TacPiece;
import com.codefish.game.core.piece.TicPiece;

/**
 * 描述
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/24 上午 11:17
 */
public class GridUtils {
    public static TicTacToeGrid copyOf(TicTacToeGrid oriGrid){
        AbstractPiece[][] pieces = oriGrid.getPieces();
        AbstractPiece[][] pieces1 = new AbstractPiece[pieces.length][pieces[0].length];
        for (int i = 0; i < pieces1.length; i++) {
            for (int j = 0; j < pieces1[i].length; j++) {
                if("A".equals(pieces[i][j].getName())){
                    pieces1[i][j] = new TicPiece();
                }
                if("B".equals(pieces[i][j].getName())){
                    pieces1[i][j] = new TacPiece();
                }
            }
        }
        TicTacToeGrid newGrid = new TicTacToeGrid();
        newGrid.setPieces(pieces1);
        return newGrid;
    }
}
