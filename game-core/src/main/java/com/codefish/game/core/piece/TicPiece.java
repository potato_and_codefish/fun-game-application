package com.codefish.game.core.piece;

/**
 * 井字棋子类1
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 01:59
 */
public class TicPiece extends AbstractPiece {

    public TicPiece(){
        super("A");
    }

    @Override
    public String toString() {
        return name;
    }
}
