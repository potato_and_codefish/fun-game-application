package com.codefish.game.core;

import com.codefish.game.core.ai.SmartAI;
import com.codefish.game.core.chess.TicTacToeGrid;
import com.codefish.game.core.piece.TicPiece;

import java.util.Scanner;

/**
 * 核心功能测试,可以脱离web环境进行游戏核心的测试
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 06:06
 */
@Deprecated
public class DemoTestApp {
    public static void main(String[] args) {
        TicTacToeGrid grid = new TicTacToeGrid();
        SmartAI ai = new SmartAI();
        Scanner in = new Scanner(System.in);
        grid.print();
        int judge = 0;
        while (true) {
            System.out.print("你的下棋位置：");
            int x = in.nextInt(), y = in.nextInt();
            grid.placePiece(x, y, new TicPiece());
            grid.print();
            judge = grid.judge();
            switch (judge) {
                case TicTacToeGrid.RED_WIN: {
                    System.out.println("你赢了！！！");
                    break;
                }
                case TicTacToeGrid.BLUE_WIN: {
                    System.out.println("AI赢了！！！");
                    break;
                }
                case TicTacToeGrid.NO_WIN: {
                    System.out.println("平局！！！");
                    break;
                }
            }
            if (judge != TicTacToeGrid.CANT_JUDGE) {
                break;
            }

            int[] pos = ai.nextStep(grid);
            System.out.println("AI的下棋位置：" + pos[0] + " " + pos[1]);
            grid.print();
            judge = grid.judge();
            switch (judge) {
                case TicTacToeGrid.RED_WIN: {
                    System.out.println("你赢了！！！");
                    break;
                }
                case TicTacToeGrid.BLUE_WIN: {
                    System.out.println("AI赢了！！！");
                    break;
                }
                case TicTacToeGrid.NO_WIN: {
                    System.out.println("平局！！！");
                    break;
                }
            }
            if (judge != TicTacToeGrid.CANT_JUDGE) {
                break;
            }
        }

    }

}
