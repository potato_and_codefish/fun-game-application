package com.codefish.game.core.piece;

import lombok.Data;

/**
 * 井字棋子类2
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 01:59
 */

public class TacPiece extends AbstractPiece{
    public TacPiece(){
        super("B");
    }

    @Override
    public String toString() {
        return name;
    }
}
