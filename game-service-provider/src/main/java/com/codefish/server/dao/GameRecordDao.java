package com.codefish.server.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.codefish.api.entities.GameRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * 描述
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/25 下午 05:51
 */
@Mapper
public interface GameRecordDao extends BaseMapper<GameRecord> {

}
