package com.codefish.server.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 游戏相关参数类
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/23 下午 10:54
 */
@Component
@ConfigurationProperties(prefix = "codefish.game")
@Data
public class GameProperties {
    /**
     * 对局最大限制时间，默认为5min
     */
    int expiresMinute = 5;
}
