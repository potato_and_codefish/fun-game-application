package com.codefish.server.config;

import com.codefish.game.core.ai.SmartAI;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 描述
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 07:50
 */
@Configuration
@MapperScan("com.codefish.server.dao")
public class ApplicationContextConfig {
    /**
     * 注册一个AI
     * @return AI示例对象
     */
    @Bean
    SmartAI smartAI() {
        return new SmartAI();
    }


}
