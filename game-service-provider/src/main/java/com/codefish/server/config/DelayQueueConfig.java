package com.codefish.server.config;

import com.codefish.server.config.properties.GameProperties;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 延时消息队列配置类
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/26 下午 03:14
 */
@Configuration
public class DelayQueueConfig {
    public static final String SENDER_EXCHANGE_NAME = "game.sender.exchange";
    public static final String MIDDLE_EXCHANGE_NAME = "game.middle.exchange";
    public static final String MIDDLE_QUEUE_NAME = "game.middle.queue";
    public static final String RECEIVER_QUEUE_NAME = "game.receiver.queue";
    public static final String SENDER_ROUTING_KEY = "game.KK";
    @Autowired
    private GameProperties gameProperties;

    @Bean
    protected DirectExchange senderExchange() {
        return ExchangeBuilder.directExchange(SENDER_EXCHANGE_NAME).durable(true).build();
    }

    @Bean
    protected FanoutExchange middleExchange() {
        return ExchangeBuilder.fanoutExchange(MIDDLE_EXCHANGE_NAME).durable(true).build();
    }

    @Bean
    protected Queue middleQueue() {
        return QueueBuilder.durable(MIDDLE_QUEUE_NAME)
                .ttl(gameProperties.getExpiresMinute() * 60 * 1000)
                .deadLetterExchange(MIDDLE_EXCHANGE_NAME)
                .build();
    }

    @Bean
    protected Queue receiverQueue() {
        return QueueBuilder.durable(RECEIVER_QUEUE_NAME).build();
    }

    @Bean
    protected Binding middleQueueToSenderExchange(DirectExchange senderExchange, Queue middleQueue) {
        return BindingBuilder.bind(middleQueue).to(senderExchange).with(SENDER_ROUTING_KEY);
    }

    @Bean
    protected Binding receiverQueueToMiddleExchange(FanoutExchange middleExchange, Queue receiverQueue) {
        return BindingBuilder.bind(receiverQueue).to(middleExchange);
    }


}
