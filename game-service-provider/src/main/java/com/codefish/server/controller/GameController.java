package com.codefish.server.controller;

import com.codefish.api.controller.IGameController;
import com.codefish.api.entities.AjaxResult;
import com.codefish.api.entities.ChessLayout;
import com.codefish.api.entities.GameRecord;
import com.codefish.api.entities.GameStatus;
import com.codefish.api.service.IGameMessageQueueService;
import com.codefish.api.service.IGameRecordService;
import com.codefish.api.service.IGameService;
import com.codefish.game.core.chess.TicTacToeGrid;
import com.codefish.game.core.piece.TicPiece;
import com.codefish.server.config.properties.GameProperties;
import com.codefish.server.service.RedisService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.UUID;

/**
 * 描述
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 07:44
 */
@RestController
@RequestMapping("/game")
@Api(tags = "game")
@Slf4j
public class GameController implements IGameController {
    @Autowired
    IGameService gameService;
    @Autowired
    RedisService redisService;
    @Autowired
    GameProperties gameProperties;
    @Autowired
    IGameMessageQueueService gameMessageQueueService;

    IGameRecordService gameRecordService;

    @Autowired
    public void setGameRecordService(IGameRecordService gameRecordService) {
        this.gameRecordService = gameRecordService;
    }


    @Override
    @GetMapping("/start")
    public AjaxResult startGame() {
        TicTacToeGrid grid = gameService.startGame();
        String token = UUID.randomUUID().toString();
        //新的grid写回redis
        redisService.setObj(token, grid);
        //启动消息队列计时
        gameMessageQueueService.startRound(token);
        //生成新的对局记录并写入数据库
        GameRecord newGameRecord = new GameRecord();
        newGameRecord.setStartTime(new Date(System.currentTimeMillis()));
        newGameRecord.setSerial(token);
        newGameRecord.setResult(GameStatus.GAMING);
        boolean res = gameRecordService.addGameRecord(newGameRecord);
        if (res) {
            log.info("添加对局记录成功:{}", newGameRecord);
        } else {
            log.warn("添加对局记录失败:{}", newGameRecord);
        }

        ChessLayout layout = new ChessLayout();
        layout.setGrid(grid.parse());
        layout.setGameCode(grid.judge());
        layout.setToken(token);
        log.info(token);
        return AjaxResult.success("开始新棋局成功", layout);
    }

    @Override
    @PostMapping("/next")
    public AjaxResult placePiece(Integer locationX, Integer locationY, String token) {
        //返回的布局对象
        ChessLayout layout = new ChessLayout();
        //游戏情况
        int gameCode = 0;
        //根据token从redis中查找棋局
        TicTacToeGrid grid = (TicTacToeGrid) redisService.getObj(token);

        if (grid == null) {
            return AjaxResult.fail("未找到棋局,代码：" + token);
        }

        //grid被玩家更新
        grid.placePiece(locationX, locationY, new TicPiece());
        //判断胜负
        gameCode = grid.judge();
        if (gameCode != TicTacToeGrid.CANT_JUDGE) {
            updateGameResult(token,gameCode);
            layout.setGrid(grid.parse());
            layout.setGameCode(gameCode);
            layout.setToken(token);
            return AjaxResult.success("下棋成功", layout);
        }

        //grid被AI更新
        gameService.nextStep(grid);
        //更新后的grid写回redis
        redisService.setObj(token, grid);


        //判断胜负
        gameCode = grid.judge();
        updateGameResult(token,gameCode);
        layout.setGrid(grid.parse());
        layout.setGameCode(gameCode);
        layout.setToken(token);

        return AjaxResult.success("下棋成功", layout);
    }



    private void updateGameResult(String token, int gameCode) {
        String status = null;
        switch (gameCode) {
            case TicTacToeGrid.RED_WIN: {
                status = GameStatus.PLAYER_WIN;
                break;
            }
            case TicTacToeGrid.BLUE_WIN: {
                status = GameStatus.AI_WIN;
                break;

            }
            case TicTacToeGrid.NO_WIN: {
                status = GameStatus.NO_WIN;
                break;
            }
            case TicTacToeGrid.CANT_JUDGE: {
                status = GameStatus.GAMING;
            }
            default:
                break;
        }
        if (status != null) {
            boolean res = gameRecordService.setResultByToken(token, status);
            if (res) {
                log.info("更新对局情况成功: token：{}，status:{}", token, status);
            } else {
                log.warn("添加对局情况失败: token：{}", token);
            }
        }
    }

}
