package com.codefish.server.listener;

import com.codefish.api.service.IGameMessageQueueService;
import com.codefish.server.config.DelayQueueConfig;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 描述
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/26 下午 03:52
 */
@Component
public class TimeOutListener {
    @Autowired
    IGameMessageQueueService messageQueueService;

    @RabbitListener(queues = DelayQueueConfig.RECEIVER_QUEUE_NAME)
    private void action(Message message, Channel channel){
        messageQueueService.timeoutAction(new String(message.getBody()));
    }
}
