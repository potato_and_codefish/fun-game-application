package com.codefish.server;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 服务提供者
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 06:51
 */
@SpringBootApplication
@EnableEurekaClient
@EnableSwagger2
@EnableRabbit
public class ServerProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServerProviderApplication.class, args);
    }
}
