package com.codefish.server.service;

import com.codefish.api.service.IGameService;
import com.codefish.game.core.ai.SmartAI;
import com.codefish.game.core.chess.TicTacToeGrid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 描述
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 07:46
 */
@Service
@Slf4j
public class GameService implements IGameService {
    @Autowired
    SmartAI smartAI;


    @Override
    public void nextStep(TicTacToeGrid grid) {
        smartAI.nextStep(grid);
    }

    @Override
    public TicTacToeGrid startGame() {
        return new TicTacToeGrid();
    }

}
