package com.codefish.server.service;

import com.codefish.api.service.IGameMessageQueueService;
import com.codefish.server.config.DelayQueueConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 描述
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/26 下午 03:39
 */
@Service
@Slf4j
public class GameMessageQueueService implements IGameMessageQueueService {
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Autowired
    GameRecordService gameRecordService;
    @Autowired
    RedisService redisService;

    @Override
    public void startRound(String token) {
        rabbitTemplate.convertAndSend(DelayQueueConfig.SENDER_EXCHANGE_NAME, DelayQueueConfig.SENDER_ROUTING_KEY, token);
        log.info("游戏{}开始计时",token);
    }

    @Override
    public void timeoutAction(String token) {
        //redis中移除对局
        redisService.removeObj(token);
        //mysql中结算
        gameRecordService.timeoutAction(token);
        log.info("游戏{}结束计时",token);
    }
}
