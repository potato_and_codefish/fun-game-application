package com.codefish.server.service;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.codefish.api.entities.GameRecord;
import com.codefish.api.entities.GameStatus;
import com.codefish.api.service.IGameRecordService;
import com.codefish.server.dao.GameRecordDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 描述
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/25 下午 05:48
 */
@Service
public class GameRecordService implements IGameRecordService {
    @Autowired
    GameRecordDao gameRecordDao;

    @Override
    public boolean setResultByToken(String token, String result) {
        UpdateWrapper<GameRecord> wrapper = new UpdateWrapper<>();
        wrapper.set("result", result).eq("serial", token);
        if (!GameStatus.GAMING.equals(result)) {
            //游戏完结，需要更新结束时间
            wrapper.set("end_time", new Date(System.currentTimeMillis()));
        }
        int updateLine = gameRecordDao.update(null, wrapper);
        return updateLine == 1;
    }

    @Override
    public boolean addGameRecord(GameRecord record) {
        int res = gameRecordDao.insert(record);
        return res == 1;
    }

    @Override
    public boolean timeoutAction(String token) {
        UpdateWrapper<GameRecord> wrapper = new UpdateWrapper<>();
        wrapper.set("result", GameStatus.TIMEOUT)
                .set("end_time", new Date(System.currentTimeMillis()))
                .eq("serial", token)
                .eq("result", GameStatus.GAMING);
        int res = gameRecordDao.update(null, wrapper);
        return res == 1;
    }

}
