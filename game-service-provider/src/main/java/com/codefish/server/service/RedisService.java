package com.codefish.server.service;

import com.codefish.api.service.IRedisService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * 描述
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 08:16
 */
@Service
public class RedisService implements IRedisService {

    @Resource
    RedisTemplate<String, Object> redisTemplate;


    @Override
    public Object getObj(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    @Override
    public void setObj(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }

    @Override
    public void setObj(String key, Object value, Long timeout) {
        redisTemplate.opsForValue().set(key,value,timeout, TimeUnit.SECONDS);
    }

    @Override
    public void removeObj(String key) {
        redisTemplate.delete(key);
    }
}
