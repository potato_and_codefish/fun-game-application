package com.codefish.api.service;

import com.codefish.game.core.chess.TicTacToeGrid;


/**
 * 描述
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 07:45
 */
public interface IGameService {
    /**
     * 根据棋局进行下一步决策
     * @param grid 棋局
     */
    public void nextStep(TicTacToeGrid grid);

    /**
     * 开始一轮新的游戏
     * @return 兄的初始棋局
     */
    public TicTacToeGrid startGame();
}
