package com.codefish.api.service;

/**
 * 消息队列服务类
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/26 下午 03:36
 */
public interface IGameMessageQueueService {
    /**
     * 开始游戏计时
     * @param token 游戏的token
     */
    void startRound(String token);

    /**
     * 游戏超时处理
     * @param token 游戏token
     */
    void timeoutAction(String token);

}
