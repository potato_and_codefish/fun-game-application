package com.codefish.api.service;

/**
 * 描述
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 08:16
 */
public interface IRedisService {

    Object getObj(String key);

    void setObj(String key, Object value);

    void setObj(String key, Object value, Long timeout);

    void removeObj(String key);

}
