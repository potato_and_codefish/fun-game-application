package com.codefish.api.service;


import com.codefish.api.entities.GameRecord;

/**
 * 对局记录服务接口
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/23 下午 09:44
 */
public interface IGameRecordService {
    /**
     * 根据token更新游戏对局情况
     *
     * @param token  游戏的token
     * @param result 新的对局情况
     * @return 更新结果
     */
    public boolean setResultByToken(String token, String result);

    /**
     * 添加新的对局记录到数据库
     *
     * @param record 新的记录
     * @return 添加结果
     */
    public boolean addGameRecord(GameRecord record);

    /**
     * mq延迟消息出发时若token对应的对局仍在游戏中，则结束它
     *
     * @param token 游戏的token
     * @return 处理结果
     */
    public boolean timeoutAction(String token);

}
