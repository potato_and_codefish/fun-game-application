package com.codefish.api.entities;

import lombok.Data;

/**
 * 描述
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 07:28
 */
@Data
public class ChessLayout {
    String[][] grid;
    String token;
    /**
     * 对局情况
     * @see com.codefish.game.core.chess.AbstractGrid
     * */
    int gameCode;

}
