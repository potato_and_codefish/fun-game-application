package com.codefish.api.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 描述
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 07:12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AjaxResult {
    int code;
    String message;
    Object data;

    public static AjaxResult success(String message) {
        return new AjaxResult(200, message, null);
    }

    public static AjaxResult success(String message, Object data) {
        return new AjaxResult(200, message, data);
    }

    public static AjaxResult fail(String message) {
        return new AjaxResult(444, message, null);
    }

    public static AjaxResult fail(int code, String message) {
        return new AjaxResult(code, message, null);
    }

    public static AjaxResult fail(int code, String message, Object data) {
        return new AjaxResult(code, message, data);
    }

    public static AjaxResult fail(String message, Object data) {
        return new AjaxResult(444, message, data);
    }


}
