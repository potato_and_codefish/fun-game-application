package com.codefish.api.entities;

/**
 * 对局状态
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/25 下午 06:54
 */
public class GameStatus {
    public static final String GAMING = "游戏中";
    public static final String TIMEOUT = "已超时";
    public static final String AI_WIN = "电脑胜";
    public static final String PLAYER_WIN = "玩家胜";
    public static final String NO_WIN = "平局";
}
