package com.codefish.api.entities;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 对局记录实体类
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/23 下午 09:55
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GameRecord {
    /**
     * 记录id
     */
    @TableId(type = IdType.AUTO)
    Long id;
    /**
     * 游戏对局token
     */
    String serial;
    /**
     * 游戏开局时间
     */
    Date startTime;
    /**
     * 游戏结束时间
     */
    Date endTime;
    /**
     * 对局结果
     */
    String result;
}
