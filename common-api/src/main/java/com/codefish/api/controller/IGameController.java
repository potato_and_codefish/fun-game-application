package com.codefish.api.controller;

import com.codefish.api.entities.AjaxResult;

/**
 * game控制类接口
 *
 * @author codefish
 * @version 1.0
 * @date 2022/05/22 下午 07:09
 */
public interface IGameController {

    /**
     * 开始游戏，服务器生成一个识别棋局的token并返回给客户端
     *
     * @return 携带token的响应
     */
    AjaxResult startGame();

    /**
     * 玩家放置棋子
     *
     * @param locationX px
     * @param locationY py
     * @param token     对应棋局的token
     * @return AI完成放置棋子后的棋局
     */
    AjaxResult placePiece(Integer locationX, Integer locationY, String token);



}
